#include "pac195x.h"
#include "esphome/core/log.h"
#include "esphome/core/hal.h"

namespace esphome {
namespace pac195x {

static const char *const TAG = "pac195x";

static const uint8_t  PAC195X_REG_REFRESH               = 0x00;
static const uint8_t  PAC195X_REG_CTRL                  = 0x01;
static const uint8_t  PAC195X_REG_ACC_COUNT             = 0x02;
static const uint8_t  PAC195X_REG_VACC_CH1              = 0x03;
static const uint8_t  PAC195X_REG_VACC_CH2              = 0x04;
static const uint8_t  PAC195X_REG_VACC_CH3              = 0x05;
static const uint8_t  PAC195X_REG_VACC_CH4              = 0x06;
static const uint8_t  PAC195X_REG_VBUS_CH1              = 0x07;
static const uint8_t  PAC195X_REG_VBUS_CH2              = 0x08;
static const uint8_t  PAC195X_REG_VBUS_CH3              = 0x09;
static const uint8_t  PAC195X_REG_VBUS_CH4              = 0x0A;
static const uint8_t  PAC195X_REG_VSENSE_CH1            = 0x0B;
static const uint8_t  PAC195X_REG_VSENSE_CH2            = 0x0C;
static const uint8_t  PAC195X_REG_VSENSE_CH3            = 0x0D;
static const uint8_t  PAC195X_REG_VSENSE_CH4            = 0x0E;
static const uint8_t  PAC195X_REG_VBUS_CH1_AVG          = 0x0F;
static const uint8_t  PAC195X_REG_VBUS_CH2_AVG          = 0x10;
static const uint8_t  PAC195X_REG_VBUS_CH3_AVG          = 0x11;
static const uint8_t  PAC195X_REG_VBUS_CH4_AVG          = 0x12;
static const uint8_t  PAC195X_REG_VSENSE_CH1_AVG        = 0x13;
static const uint8_t  PAC195X_REG_VSENSE_CH2_AVG        = 0x14;
static const uint8_t  PAC195X_REG_VSENSE_CH3_AVG        = 0x15;
static const uint8_t  PAC195X_REG_VSENSE_CH4_AVG        = 0x16;
static const uint8_t  PAC195X_REG_VPOWER_CH1            = 0x17;
static const uint8_t  PAC195X_REG_VPOWER_CH2            = 0x18;
static const uint8_t  PAC195X_REG_VPOWER_CH3            = 0x19;
static const uint8_t  PAC195X_REG_VPOWER_CH4            = 0x1A;
static const uint8_t  PAC195X_REG_SMBUS_CFG             = 0x1C;
static const uint8_t  PAC195X_REG_NEG_PWR_FSR           = 0x1D;
static const uint8_t  PAC195X_REG_REFRESH_G             = 0x1E;
static const uint8_t  PAC195X_REG_REFRESH_V             = 0x1F;
static const uint8_t  PAC195X_REG_SLOW                  = 0x20;
static const uint8_t  PAC195X_REG_CTRL_ACT              = 0x21;
static const uint8_t  PAC195X_REG_NEG_PWR_FSR_ACT       = 0x22;
static const uint8_t  PAC195X_REG_CTRL_LAT              = 0x23;
static const uint8_t  PAC195X_REG_NEG_PWR_FSR_LAT       = 0x24;
static const uint8_t  PAC195X_REG_ACC_CFG               = 0x25;
static const uint8_t  PAC195X_REG_ALERT_STATUS          = 0x26;
static const uint8_t  PAC195X_REG_SLOW_ALERT1           = 0x27;
static const uint8_t  PAC195X_REG_GPIO_ALERT2           = 0x28;
static const uint8_t  PAC195X_REG_ACC_FULLNESS_LIM      = 0x29;
static const uint8_t  PAC195X_REG_OC_LIM_CH1            = 0x30;
static const uint8_t  PAC195X_REG_OC_LIM_CH2            = 0x31;
static const uint8_t  PAC195X_REG_OC_LIM_CH3            = 0x32;
static const uint8_t  PAC195X_REG_OC_LIM_CH4            = 0x33;
static const uint8_t  PAC195X_REG_UC_LIM_CH1            = 0x34;
static const uint8_t  PAC195X_REG_UC_LIM_CH2            = 0x35;
static const uint8_t  PAC195X_REG_UC_LIM_CH3            = 0x36;
static const uint8_t  PAC195X_REG_UC_LIM_CH4            = 0x37;
static const uint8_t  PAC195X_REG_OP_LIM_CH1            = 0x38;
static const uint8_t  PAC195X_REG_OP_LIM_CH2            = 0x39;
static const uint8_t  PAC195X_REG_OP_LIM_CH3            = 0x3A;
static const uint8_t  PAC195X_REG_OP_LIM_CH4            = 0x3B;
static const uint8_t  PAC195X_REG_OV_LIM_CH1            = 0x3C;
static const uint8_t  PAC195X_REG_OV_LIM_CH2            = 0x3D;
static const uint8_t  PAC195X_REG_OV_LIM_CH3            = 0x3E;
static const uint8_t  PAC195X_REG_OV_LIM_CH4            = 0x3F;
static const uint8_t  PAC195X_REG_UV_LIM_CH1            = 0x40;
static const uint8_t  PAC195X_REG_UV_LIM_CH2            = 0x41;
static const uint8_t  PAC195X_REG_UV_LIM_CH3            = 0x42;
static const uint8_t  PAC195X_REG_UV_LIM_CH4            = 0x43;
static const uint8_t  PAC195X_REG_OC_LIM_NSAMPLES       = 0x44;
static const uint8_t  PAC195X_REG_UC_LIM_NSAMPLES       = 0x45;
static const uint8_t  PAC195X_REG_OP_LIM_NSAMPLES       = 0x46;
static const uint8_t  PAC195X_REG_OV_LIM_NSAMPLES       = 0x47;
static const uint8_t  PAC195X_REG_UV_LIM_NSAMPLES       = 0x48;
static const uint8_t  PAC195X_REG_ALERT_ENABLE          = 0x49;
static const uint8_t  PAC195X_REG_ACC_CFG_ACT           = 0x4A;
static const uint8_t  PAC195X_REG_ACC_CFG_LAT           = 0x4B;
static const uint8_t  PAC195X_REG_ID_PRODUCT            = 0xFD;
static const uint8_t  PAC195X_REG_ID_MANUFACTURER       = 0xFE;
static const uint8_t  PAC195X_REG_ID_REVISION           = 0xFF;
static const uint8_t  PAC1951_1_ID                      = 0x78;
static const uint8_t  PAC1952_1_ID                      = 0x79;
static const uint8_t  PAC1953_1_ID                      = 0x7A;
static const uint8_t  PAC1954_1_ID                      = 0x7B;
static const uint8_t  PAC1951_2_ID                      = 0x7C;
static const uint8_t  PAC1952_2_ID                      = 0x7D;
static const uint8_t  PAC195X_FSR_V                     = 32;
static const float    PAC195X_VOLTAGE_LSB               = 32.0 / 65536.0;   // 32V FSR / 2^16
static const float    PAC195X_VSENSE_LSB                = 0.100 / 65536.0;  // 100mV FSR / 2^16

// Addresses:
// RESISTOR (1%) SMBus Address
// 0 (GND)     0010 000 (R/W) -> 0x10
// 499         0010 001 (R/W) -> 0x11
// 806         0010 010 (R/W) -> 0x12
// 1,270       0010 011 (R/W) -> 0x13
// 2,050       0010 100 (R/W) -> 0x14
// 3,240       0010 101 (R/W) -> 0x15
// 5,230       0010 110 (R/W) -> 0x16
// 8,450       0010 111 (R/W) -> 0x17
// 13,300      0011 000 (R/W) -> 0x18
// 21,500      0011 001 (R/W) -> 0x19
// 34,000      0011 010 (R/W) -> 0x1A
// 54,900      0011 011 (R/W) -> 0x1B
// 88,700      0011 100 (R/W) -> 0x1C
// 140,000     0011 101 (R/W) -> 0x1D
// 226,000     0011 110 (R/W) -> 0x1E
// Tie to VDD  0011 111 (R/W) -> 0x1F

void PAC195XComponent::setup() {
  ESP_LOGCONFIG(TAG, "Setting up PAC195x ...");
  // Config Register
  uint8_t response_ = 0;
  this->read_byte(PAC195X_REG_ID_PRODUCT, &response_);
  switch (response_){
    case PAC1951_1_ID:
      ESP_LOGD(TAG, "Detected PAC1951-1 with 1 channel");
      supported_channels = 0b0001;
      break;
    case PAC1952_1_ID:
      ESP_LOGD(TAG, "Detected PAC1952-1 with 2 channels");
      supported_channels = 0b0011;
      break;
    case PAC1953_1_ID:
      ESP_LOGD(TAG, "Detected PAC1953-1 with 3 channels");
      supported_channels = 0b0111;
      break;
    case PAC1954_1_ID:
      ESP_LOGD(TAG, "Detected PAC1954-1 with 4 channels");
      supported_channels = 0b1111;
      break;
    case PAC1951_2_ID:
      ESP_LOGD(TAG, "Detected PAC1951-2 with 1 channel");
      supported_channels = 0b0001;
      break;
    case PAC1952_2_ID:
      ESP_LOGD(TAG, "Detected PAC1952-2 with 2 channels");
      supported_channels = 0b0011;
      break;
    default:
      this->mark_failed();
      return;
  }
  delay(1);

  uint16_t config = 0x0700;
  // Dissable unused channels if not used. 1 = OFF
  if (!this->channels_[0].exists()) {
    config |= 0x80;
    ESP_LOGD(TAG, "Setting unused Channel 1 to disabled...");
  }
  if ((!this->channels_[1].exists()) || !(supported_channels & 2)) {
    config |= 0x40;
    ESP_LOGD(TAG, "Setting unused or unsupported Channel 2 to disabled...");
  }
  if ((!this->channels_[2].exists()) || !(supported_channels & 4)) {
    config |= 0x20;
    ESP_LOGD(TAG, "Setting unused or unsupported Channel 3 to disabled...");
  }
  if ((!this->channels_[3].exists()) || !(supported_channels & 8)) {
    config |= 0x10;
    ESP_LOGD(TAG, "Setting unused or unsupported Channel 4 to disabled...");
  }

  if (!this->write_byte_16(PAC195X_REG_CTRL, config)) {
    this->mark_failed();
    return;
  }
}

void PAC195XComponent::dump_config() {
  ESP_LOGCONFIG(TAG, "PAC195X:");
  LOG_I2C_DEVICE(this);
  if (this->is_failed()) {
    ESP_LOGE(TAG, "Communication with PAC195x failed!");
  }
  LOG_UPDATE_INTERVAL(this);

  LOG_SENSOR("  ", "Bus Voltage #1", this->channels_[0].bus_voltage_sensor_);
  LOG_SENSOR("  ", "Shunt Voltage #1", this->channels_[0].shunt_voltage_sensor_);
  LOG_SENSOR("  ", "Current #1", this->channels_[0].current_sensor_);
  LOG_SENSOR("  ", "Power #1", this->channels_[0].power_sensor_);
  LOG_SENSOR("  ", "Bus Voltage #2", this->channels_[1].bus_voltage_sensor_);
  LOG_SENSOR("  ", "Shunt Voltage #2", this->channels_[1].shunt_voltage_sensor_);
  LOG_SENSOR("  ", "Current #2", this->channels_[1].current_sensor_);
  LOG_SENSOR("  ", "Power #2", this->channels_[1].power_sensor_);
  LOG_SENSOR("  ", "Bus Voltage #3", this->channels_[2].bus_voltage_sensor_);
  LOG_SENSOR("  ", "Shunt Voltage #3", this->channels_[2].shunt_voltage_sensor_);
  LOG_SENSOR("  ", "Current #3", this->channels_[2].current_sensor_);
  LOG_SENSOR("  ", "Power #3", this->channels_[2].power_sensor_);
  LOG_SENSOR("  ", "Bus Voltage #4", this->channels_[3].bus_voltage_sensor_);
  LOG_SENSOR("  ", "Shunt Voltage #4", this->channels_[3].shunt_voltage_sensor_);
  LOG_SENSOR("  ", "Current #4", this->channels_[3].current_sensor_);
  LOG_SENSOR("  ", "Power #4", this->channels_[3].power_sensor_);
}

inline uint8_t pac195x_bus_voltage_register(int channel) { return PAC195X_REG_VBUS_CH1 + channel; }

inline uint8_t pac195x_shunt_voltage_register(int channel) { return PAC195X_REG_VSENSE_CH1 + channel; }

inline uint8_t pac195x_power_register(int channel) { return PAC195X_REG_VPOWER_CH1 + channel; }

// bool pac195x_channel_supported(int channel, uint8_t chip){
//   switch(chip){
//     case: ID_PAC1952_1
//     case: ID_PAC1952_2
//     case: ID_PAC1953_1
//     case: ID_PAC1954_1
//     {
//       if(channel <= 2)
//       {
//         return true;
//       }
//     }
//     case: ID_PAC1953_1
//     case: ID_PAC1954_1
//     {
//       if(channel <= 3)
//       {
//         return true;
//       }
//     }
//     case: ID_PAC1954_1
//     {
//       if(channel <= 4)
//       {
//         return true;
//       }
//     }
//     default:
//       return flase;
//   }
// }

void PAC195XComponent::update() {
  int update_start, begin_loop, read_bv, bv_math, bv_publish, bv_done, read_sv, sv_math, sv_publish, a_publish, w_publish, final_ts, last_millis, new_millis;
  update_start = millis();
  this->refresh_v_sensors();

  delay(1); // Wait 1ms for samples to be ready.

  for (int i = 0; i < 4; i++) {
    read_bv = bv_math = bv_publish = bv_done = read_sv = sv_math = sv_publish = a_publish = w_publish = final_ts = 0;
    new_millis = millis();    // Load loop start
    begin_loop = new_millis;
    last_millis = new_millis; // Reset with each loop
    PAC195XChannel &channel = this->channels_[i];
    float bus_voltage_v = NAN, current_a = NAN, power_w = NAN;
    uint16_t raw;
    if (channel.should_measure_bus_voltage()) {
      if (!this->read_byte_16(pac195x_bus_voltage_register(i), &raw)) {
        this->status_set_warning();
        return;
      }
      new_millis = millis();
      read_bv = new_millis - last_millis;
      last_millis = new_millis;
      bus_voltage_v = int16_t(raw) * PAC195X_VOLTAGE_LSB;
      new_millis = millis();
      bv_math = new_millis - last_millis;
      last_millis = new_millis;
      if (channel.bus_voltage_sensor_ != nullptr)
        channel.bus_voltage_sensor_->publish_state(bus_voltage_v);
        new_millis = millis();
        bv_publish = new_millis - last_millis;
        last_millis = new_millis;
    }
      new_millis = millis();
      bv_done = new_millis - last_millis;
      last_millis = new_millis;
    // ESP_LOGD(TAG, "Bus voltage sensor done @ %d", millis());
    if (channel.should_measure_shunt_voltage()) {
      if (!this->read_byte_16(pac195x_shunt_voltage_register(i), &raw)) {
        this->status_set_warning();
        return;
      }
      new_millis = millis();
      read_sv = new_millis - last_millis;
      last_millis = new_millis;
      const float shunt_voltage_v = int16_t(raw) * PAC195X_VSENSE_LSB;
      current_a = shunt_voltage_v / channel.shunt_resistance_;
      new_millis = millis();
      sv_math = new_millis - last_millis;
      last_millis = new_millis;
      if (channel.shunt_voltage_sensor_ != nullptr)
        channel.shunt_voltage_sensor_->publish_state(shunt_voltage_v);
        new_millis = millis();
        sv_publish = new_millis - last_millis;
        last_millis = new_millis;
      if (channel.current_sensor_ != nullptr)
        channel.current_sensor_->publish_state(current_a);
        new_millis = millis();
        a_publish = new_millis - last_millis;
        last_millis = new_millis;
    }
    if (channel.power_sensor_ != nullptr) {
      channel.power_sensor_->publish_state(bus_voltage_v * current_a);
      new_millis = millis();
      w_publish = new_millis - last_millis;
      last_millis = new_millis;
    }

    // if (channel.should_measure_power()) {
    //   if (!this->read_byte_16(pac195x_power_register(i), &raw)) {
    //     this->status_set_warning();
    //     return;
    //   }
    //   // power_w = int16_t(raw) / 1000.0f;
    //   if (channel.power_sensor_ != nullptr)
    //     channel.power_sensor_->publish_state(power_w);
    // }
    new_millis = millis();
    final_ts = new_millis - last_millis;
    // ESP_LOGI(TAG, "\nUpdate_start: %d\nbegin_loop: %d\nread_bv: %d\nbv_math: %d\nbv_publish: %d\nbv_done: %d\nread_sv: %d\nsv_math: %d\nsv_publish: %d\na_publish: %d\nw_publish: %d\nfinal_ts: %d\n", update_start, begin_loop, read_bv, bv_math, bv_publish, bv_done, read_sv, sv_math, sv_publish, a_publish, w_publish, final_ts);
  }
}

float PAC195XComponent::get_setup_priority() const { return setup_priority::DATA; }
void PAC195XComponent::set_shunt_resistance(int channel, float resistance_ohm) {
  this->channels_[channel].shunt_resistance_ = resistance_ohm;
}

void PAC195XComponent::refresh_v_sensors(void){
  uint8_t c = PAC195X_REG_REFRESH_V;
  if (this->write(&c, 1) != i2c::NO_ERROR) {
     this->mark_failed();
     return;
  }
}

void PAC195XComponent::refresh_sensors(void){
  uint8_t c = PAC195X_REG_REFRESH;
  if (this->write(&c, 1) != i2c::NO_ERROR) {
     this->mark_failed();
     return;
  }
}

bool PAC195XComponent::PAC195XChannel::exists() {
  return this->bus_voltage_sensor_ != nullptr || this->shunt_voltage_sensor_ != nullptr ||
         this->current_sensor_ != nullptr || this->power_sensor_ != nullptr;
}
bool PAC195XComponent::PAC195XChannel::should_measure_shunt_voltage() {
  return this->shunt_voltage_sensor_ != nullptr || this->current_sensor_ != nullptr || this->power_sensor_ != nullptr;
}
bool PAC195XComponent::PAC195XChannel::should_measure_bus_voltage() {
  return this->bus_voltage_sensor_ != nullptr || this->power_sensor_ != nullptr;
}
bool PAC195XComponent::PAC195XChannel::should_measure_power() {
  return this->power_sensor_ != nullptr;
}

}  // namespace pac195x
}  // namespace esphome
